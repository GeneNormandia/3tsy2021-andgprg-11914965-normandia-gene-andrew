using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FourthButton : MonoBehaviour
{
    public GameObject bridge;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Box")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(0f, 7f);
            Destroy(this.gameObject);
        }
    }
}
