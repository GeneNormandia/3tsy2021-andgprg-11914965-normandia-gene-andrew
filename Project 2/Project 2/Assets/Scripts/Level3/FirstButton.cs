using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstButton : MonoBehaviour
{
    public GameObject bridge;
    public GameObject buttonDown;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(-1.87f, -0.536f);
            buttonDown.transform.position = new Vector3(-6.27f, -3.95f);
            Destroy(this.gameObject);
        }
    }
}


