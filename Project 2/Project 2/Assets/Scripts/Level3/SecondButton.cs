using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondButton : MonoBehaviour
{
    public GameObject bridge;
    public GameObject buttonDown;
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(-1.27f, -1.587f);
        }
    }
}
