using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdButton : MonoBehaviour
{
    public GameObject bridge;
    public GameObject bridge2;
    public GameObject buttonDown;
    

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(6.288f, -1.588f);
            bridge2.transform.position = new Vector3(-1.725f, -3.87f);
            buttonDown.transform.position = new Vector3(0.462f, -4.222f);
            Destroy(this.gameObject);
        }
    }
}
