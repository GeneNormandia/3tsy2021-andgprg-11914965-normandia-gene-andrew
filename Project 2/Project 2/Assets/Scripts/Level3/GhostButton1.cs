using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostButton1 : MonoBehaviour
{
    public GameObject bridge;
    public GameObject bridge2;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(-1.725f, -3.87f);
            bridge2.transform.position = new Vector3(3.31f, -1.593f);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            bridge.transform.position = new Vector3(-1.725f, -2.84f);
            bridge2.transform.position = new Vector3(0f, 6f);
        }
    }
}
