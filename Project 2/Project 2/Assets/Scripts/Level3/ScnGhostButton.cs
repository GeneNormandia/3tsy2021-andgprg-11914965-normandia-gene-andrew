using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScnGhostButton : MonoBehaviour
{
    public GameObject bridge;
    public GameObject bridge2;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(6f, 7f);
            bridge2.transform.position = new Vector3(6f, 7f);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            bridge.transform.position = new Vector3(4.59f, -3.81f);
        }
    }
}
