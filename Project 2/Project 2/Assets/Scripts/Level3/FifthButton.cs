using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FifthButton : MonoBehaviour
{
    public GameObject bridge;
    public GameObject buttonDown;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(-1.87f, -4.533f);
            buttonDown.transform.position = new Vector3(5.64f, -4.208f);
            Destroy(this.gameObject);
        }
    }
}
