using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button1 : MonoBehaviour
{
    public GameObject bridge;
    public GameObject buttonDown;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            SoundManager.PlaySound("bridgePop");
            bridge.transform.position = new Vector3(-0.379f, -0.182f);
            Destroy(this.gameObject);
        }
    }
}
