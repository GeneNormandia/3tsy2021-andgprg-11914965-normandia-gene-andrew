using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostButton : MonoBehaviour
{
    public GameObject bridge;
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            SoundManager.PlaySound("bridgePop"); 
            bridge.transform.position = new Vector3(0f, 7f);
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Ghost")
        {
            bridge.transform.position = new Vector3(-4.178f, -1.98f);
        }
    }
}