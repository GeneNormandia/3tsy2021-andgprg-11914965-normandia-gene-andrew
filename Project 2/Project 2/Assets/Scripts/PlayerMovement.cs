using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
	public float MovementSpeed = 1;
	private Rigidbody2D _rigidbody;
	public Vector3 startPosition;
	public GameObject shadow;
	float Timer = 0;
	float Counter = 0;

	public AudioClip audioClip;
	public AudioSource audioSource;

	void Start()
	{
		audioSource = GetComponent<AudioSource>();
		startPosition = transform.position;
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		Timer++;
		//Spawn a shadow
		if (Input.GetKeyDown("q"))
		{
			startPosition = transform.position;
			Instantiate(shadow, transform.position, transform.rotation);
			if (Counter == 0)
            {
				Counter += 1;
            }
			audioSource.PlayOneShot(audioClip);
			//SoundManager.PlaySound("bridgePop");
		}
		var movement = Input.GetAxis("Horizontal");
		transform.position += new Vector3(movement, 0, 0) * Time.deltaTime * MovementSpeed;
		//transform.position += new Vector3(movement, 0, 0) * Time.deltaTime;
		//Flip character dependeing on where they are going
		if (!Mathf.Approximately(0, movement))
        {
            transform.rotation = movement < 0 ? Quaternion.Euler(0, 180, 0) : Quaternion.identity;
        }
		//Reset the character position
		if (Timer >= 300 && Counter >= 1)
		{
			if (Input.GetKey("e"))
			{
				transform.position = startPosition;
				audioSource.PlayOneShot(audioClip);
				Timer = 0;
			}
        }
	}
}
