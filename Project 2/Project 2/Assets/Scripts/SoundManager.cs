using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip bridgeSound, setGhost;
    static AudioSource audioSrc;


    // Start is called before the first frame update
    void Start()
    {
        bridgeSound = Resources.Load<AudioClip>("bridgePop");
        setGhost = Resources.Load<AudioClip>("ghostSet");

        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "bridgePop":
                audioSrc.PlayOneShot(bridgeSound);
                break;
        }
    }
}
