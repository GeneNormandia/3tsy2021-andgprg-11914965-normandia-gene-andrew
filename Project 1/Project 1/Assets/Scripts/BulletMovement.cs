using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMovement : MonoBehaviour
{
    public float speed = 30;
    public Rigidbody rb;
    public float TimeToLive = 4f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed;
        Destroy(gameObject, TimeToLive);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
