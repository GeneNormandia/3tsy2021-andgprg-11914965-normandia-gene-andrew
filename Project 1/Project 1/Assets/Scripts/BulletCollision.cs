using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletCollision : MonoBehaviour
{
    public int hp = 3;
    public GameObject sam;
    public AudioClip audioClip;
    public AudioSource audioSource;

    public Transform nozzle;
    public GameObject soundEffect;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            hp--;

            if (hp <= 0)
            {
                Instantiate(soundEffect, nozzle.transform.position, transform.rotation);
                Destroy(this.gameObject);
                sam.GetComponent<SceneChanger>().counter++;
            }
        }
    }

    void Update()
    {

    }
}
