using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Death : MonoBehaviour
{
    public AudioClip audioClip;
    public AudioSource audioSource;
    public int counter = 0;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (counter == 3)
        {
            audioSource.PlayOneShot(audioClip);
        }
    }
}
