using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectedTower : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    public TowerBehavior curSelectTwr;

    public Text wave;  // public if you want to drag your text object in there manually


    private string name;
    private int level;
    private int damage;
    private int range;
    private int upgradeCost;





    // Start is called before the first frame update
    void Start()
    {
   /*     wave = GetComponent<Text>();  // if you want to reference it by code - tag it if you have several texts*/

    }

    // Update is called once per frame
    void Update()
    {
      /*  wave.text = level.ToString();  // make it a string to output to the Text object*/


        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if(Physics.Raycast(ray, out hit))
        {
            if(Input.GetMouseButtonDown(0))
            {
                if(hit.collider.gameObject.GetComponent<TowerBehavior>())
                {
                    Debug.Log(hit.collider.gameObject.name);
                    curSelectTwr = hit.collider.gameObject.GetComponent<TowerBehavior>();
                    curSelectTwr.TowerStats();


                    level += curSelectTwr.level;

                }
            }
        }
    }

    public void UpgradeTower()
    {
        curSelectTwr.Upgrade();
        //curSelectTwr.GetComponent<TowerBehavior>().Upgrade();
    }




}
