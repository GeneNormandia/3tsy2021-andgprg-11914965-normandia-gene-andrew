using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public float HP = 0f;
    public float gold = 0f;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        UIManager.instance.DisplayHp(HP);
        UIManager.instance.DisplayGold(gold);
    }

    // Update is called once per frame
    void Update()
    {
        UIManager.instance.DisplayHp(HP);
        UIManager.instance.DisplayGold(gold);
        if (HP <= 0)
        {
            SceneChanger.instance.GameOver();
        }
        if (gold <= 0)
        {
            gold = 0;
        }
    }
}
