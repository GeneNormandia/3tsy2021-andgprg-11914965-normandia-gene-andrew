﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public SkinnedMeshRenderer Turret; 
    public MeshRenderer Base;
    public Material originalMaterial;
    public Material redMaterial;
    public Material greenMaterial;

    public void Build()
    {
        Turret.material = originalMaterial;
        Base.material = originalMaterial;
        this.GetComponent<Collider>().enabled = true;
        this.GetComponent<BoxCollider>().enabled = true;
    }

    public void NonBuildable()
    {
        Turret.material = redMaterial;
        Base.material = redMaterial;
    }

    public void Buildable()
    {
        Turret.material = greenMaterial;
        Base.material = greenMaterial;
    }

    IEnumerator BuildTower(float timer)
    {
        float time = 0;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            yield return new WaitForSeconds(1);
        }
        Debug.Log("Tower is ready");
    }

    void Start()
    {

    }
}
