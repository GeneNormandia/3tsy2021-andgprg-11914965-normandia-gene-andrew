using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LandBoss : MonoBehaviour
{
    public bool land;
    public bool flying;

    private static int iceDur = 0;
    private static int fireDur = 0;

    NavMeshAgent agent;
    public Transform target;

    //Health scales with the wave number
    public int Hp = 1000 * WaveManager.instance.waveCount;
    //100 * WaveManager.waveCount/2

    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        agent.SetDestination(target.position);
    }

    void OnTriggerEnter(Collider collider)
    {
        iceDur += TowerBehavior.iceDuration;
        fireDur += TowerBehavior.fireDuration;
        if (collider.gameObject.CompareTag("Nexus"))
        {
            GameManager.instance.HP--;
            Debug.Log("collided");
            Hp = 0;
            SceneChanger.instance.GameOver();


        }

        else if (collider.gameObject.CompareTag("NewArrow"))
        {
            Hp -= TowerBehavior.arrowDamage;
            Debug.Log("Arrow hit" + Hp);
        }

        else if (collider.gameObject.CompareTag("Arrow"))//Cannon
        {
            Hp -= TowerBehavior.boulderDamage;
            Debug.Log("Boulder");
        }

        else if (collider.gameObject.CompareTag("Ice"))//Ice Tower
        {

            Debug.Log("Ice");
        }
        else if (collider.gameObject.CompareTag("Fire"))//Ice Tower
        {

            Debug.Log("Fire");
        }
    }

    public void hitBoulder()
    {
        Debug.Log("BOULDER IS HIT");
        Hp -= TowerBehavior.boulderDamage;
    }

    public void HitIce()
    {
        StartCoroutine(IceSlow(iceDur));
        Debug.Log("Ice");
    }

    public void HitFire()
    {
        StartCoroutine(FireBurn(fireDur));
        Debug.Log("Fire");
    }

    void Update()
    {
        //Debug.Log("HP: " + Hp);
        if (Hp <= 0)
        {
            WaveManager.instance.enemyCounter--;
            Debug.Log("ENEMY COUNTER: " + WaveManager.instance.enemyCounter);
            Destroy(this.gameObject);
            GameManager.instance.gold += 10;

        }
        //  agent.SetDestination(target.position);
    }

    IEnumerator IceSlow(float timer)
    {
        float time = 0;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            GetComponent<NavMeshAgent>().speed = TowerBehavior.iceSlow;
            yield return new WaitForSeconds(1);
        }
        GetComponent<NavMeshAgent>().speed = 3;
    }
    IEnumerator FireBurn(float timer)
    {
        float time = 0;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            Hp -= TowerBehavior.fireDamage;
            //GetComponent<TowerBehavior>().speed = TowerBehavior.iceSlow;
            yield return new WaitForSeconds(1);
        }
    }
}
