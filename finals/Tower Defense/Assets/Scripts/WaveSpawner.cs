using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveSpawner : MonoBehaviour
{
    public Transform spawn;
    //public GameObject goblinPrefab;
    public GameObject goblinPrefab;





    public static float  waveCounter = 3;
    public static float  enemiesToSpawn = waveCounter * 2;

    public float timeBetweenWaves = 5;
    private float countdown = 2f;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (countdown <= 0f)
        {
            StartCoroutine(SpawnWave());
            countdown = timeBetweenWaves; 
        }


        /* if (enemiesToSpawn > 0)
         {
             Instantiate(goblinPrefab, spawn.transform.position, transform.rotation);
             enemiesToSpawn -= 1;

         }*/
    }

    IEnumerator SpawnWave()
    {
        for (int i = 0; i < waveCounter; i++)
        {
            SpawnEnemy();

            yield return new WaitForSeconds(0.5f);
        }
        waveCounter++;
        Debug.Log("wave coming");
    }

    void SpawnEnemy()
    {
        Instantiate(goblinPrefab, spawn.transform.position, transform.rotation);
    }
}
