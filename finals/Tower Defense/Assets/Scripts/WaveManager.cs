using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;
    public Transform spawn;
    public GameObject landEnemy;
    public GameObject landBoss;
    public GameObject flyingEnemy;
    public GameObject flyingBoss;

    public  int waveCount = 1;
    public int totalWaves = 10;
    public int enemiesToSpawn = 2;
    public  int bossToSpawn = 1;
    public  int enemyCounter = 0;
    public int enemyLeft;
    public bool checker;
    private float countDown = .5f;
    private bool isEnemyDone = false;
    void Awake()
    {
        enemyLeft = enemiesToSpawn;
    }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        //SpawnWave();
        UIManager.instance.DisplayWave(waveCount);
    }

    // Update is called once per frame
    void Update()
    {
        UIManager.instance.DisplayWave(waveCount);

       // SpawnWave();
        if (countDown <= 0f)
        {
            if (enemiesToSpawn != 0)
             {
                    SpawnWave();

             }
           
            countDown = 1f;
        }
        if (enemiesToSpawn <= 0)
        {
            isEnemyDone = true;

                }
        if (enemyCounter <= 0 && isEnemyDone == true)
        {
            enemiesToSpawn = 10;
            waveCount++;
            isEnemyDone = false;
        }

        countDown -= Time.deltaTime;
    }

    void SpawnWave()
    {
            if (waveCount == 1)
            {
            //if (enemiesToSpawn != 0)
                    isEnemyDone = false;
                   // Debug.Log("Wave 1");
                    Knight();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
            Debug.Log("Enemy counter:" + enemyCounter);
            Debug.Log("Enemy to spawn:" + enemiesToSpawn);
          
            }

            if (waveCount == 2)
            {

                    isEnemyDone = false;
                    Knight();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                if (enemyCounter <= 0 && isEnemyDone == false)
                {
                    waveCount++;
                    isEnemyDone = true;
                }
            }

            if (waveCount == 3)
            {
                   
                    Knight();

                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
            isEnemyDone = false;
            if (enemyCounter <= 0 && isEnemyDone == false)
                 {
                waveCount++;
                isEnemyDone = true;
                }
         }

            if (waveCount == 4)//Boss wave
            {
            
            Knight();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
                if (isEnemyDone == true)
                {
                Debug.Log("BOSS SPAWNED");
                    KnightBoss();
                    enemyCounter += 1;
                    bossToSpawn -= 1;
                }
            isEnemyDone = false;
            if (enemyCounter <= 0 && isEnemyDone == false)
            {
                waveCount++;
                isEnemyDone = true;
            }
        }

            if (waveCount == 5)
            {

            KnightBoss();
            enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
            }

            if (waveCount == 6)
            {

                    Raven();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
            }

            if (waveCount == 7)//Boss wave
            {

                    Raven();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
                if (bossToSpawn != 0)
                {
                    Dragon();
                    enemyCounter += 1;
                    bossToSpawn -= 1;
                }
            }

            if (waveCount == 8)
            {
                    Knight();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
            }

            if (waveCount == 9)
            {

                    Raven();
                    enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
            }

            if (waveCount == 10)//Boss wave
            {

            Dragon();
            enemiesToSpawn -= 1;
                    enemyCounter += 1;
                
                if (bossToSpawn != 0)
                {
                    Dragon();
                    KnightBoss();
                    enemyCounter += 1;
                    bossToSpawn -= 1;
                }
            }
        if (waveCount > 10)
        {
            SceneChanger.instance.GameOver();
        }
    }



    
    















    void Knight()
    {
        Instantiate(landEnemy, spawn.transform.position, transform.rotation);
    }

    void KnightBoss()
    {
        Instantiate(landBoss, spawn.transform.position, transform.rotation);
    }

    void Raven()
    {
        Instantiate(flyingEnemy, spawn.transform.position, transform.rotation);
    }

    void Dragon()
    {
        Instantiate(flyingBoss, spawn.transform.position, transform.rotation);
    }
}
