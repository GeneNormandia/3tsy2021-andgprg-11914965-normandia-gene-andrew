﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
    Ray ray;
    RaycastHit hit;

    public GameObject currentTower;

    public GameObject cannon;
    public GameObject arrow;
    public GameObject fire;
    public GameObject ice;
    public GameObject cancel;
    public Button cancelButton;


    //public GameObject towerBehavior;
    public void createCannonTower()
    {
        this.GetComponent<BuildManager>().enabled = true;
        currentTower.transform.position = new Vector2(0, -0);
        currentTower = cannon;
    }

    public void createArrowTower()
    {
            this.GetComponent<BuildManager>().enabled = true;
            currentTower.transform.position = new Vector2(0, -0);
            currentTower = arrow;
    }

    public void createFireTower()
    {
        this.GetComponent<BuildManager>().enabled = true;
        currentTower.transform.position = new Vector2(0, -0);
        currentTower = fire;
    }

    public void createIceTower()
    {
        this.GetComponent<BuildManager>().enabled = true;
        currentTower.transform.position = new Vector2(0, -0);
        currentTower = ice;
    }

    public void cancelBuild()
    {
        this.GetComponent<BuildManager>().enabled = false;
        Debug.Log("CANCELLING");
        currentTower.transform.position = new Vector2(0, -0);
        currentTower = cancel;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    float minYAxis = 2;
    float maxYAxis = 2.1f;
    float minYBuildArea = 2;

    Vector3 SnapToGrid(Vector3 towerObjectPos)
    {
        return new Vector3(Mathf.Round(towerObjectPos.x),
                            Mathf.Clamp(towerObjectPos.y, minYAxis, maxYAxis),
                            Mathf.Round(towerObjectPos.z));
    }

    void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return;
        }

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            currentTower.transform.position = SnapToGrid(hit.point);
            Debug.DrawLine(ray.origin, hit.point, Color.green);

            if (hit.point.y < minYBuildArea)
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }

            else if (hit.collider.gameObject.tag == "Tower" || hit.collider.gameObject.tag == "range")
            {
                currentTower.GetComponent<Tower>().NonBuildable();
                return;
            }

            else
            {
                currentTower.GetComponent<Tower>().Buildable();
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (GameManager.instance.gold >= TowerBehavior.instance.cost)
                {
                    GameObject tempTower = (GameObject)Instantiate(currentTower, SnapToGrid(hit.point), currentTower.transform.rotation);
                    tempTower.GetComponent<Tower>().Build();
                    tempTower.GetComponent<TowerBehavior>().BuildTower();
                }
                else
                {
                    Debug.Log("Not enough gold");
                    return;
                }

            }
        }
    }

    IEnumerator BuildArrow(float timer)
    {
        float time = 0;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            yield return new WaitForSeconds(1);
        }
        GameObject tempTower = (GameObject)Instantiate(currentTower, SnapToGrid(hit.point), currentTower.transform.rotation);
        tempTower.GetComponent<Tower>().Build();
    }
}
