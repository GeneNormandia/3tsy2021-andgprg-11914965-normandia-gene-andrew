using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    private string name;
    private int level;
    private int damage;
    private int range;
    private int upgradeCost;
    //left
    public TextMeshProUGUI hpText;
    public TextMeshProUGUI goldText;
    public TextMeshProUGUI waveText;

    //right
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI damageText;
    public TextMeshProUGUI rangeText;
    public TextMeshProUGUI upgradeCostText;


    public void DisplayHp(float hp)
    {
        hpText.text = "HP: " + hp;
    }

    public void DisplayGold(float gold)
    {
        goldText.text = "Gold: " + gold;
    }

    public void DisplayWave(int wavecount)
    {
        waveText.text = "Wave: " + wavecount;
    }

    public void DisplayLevel(float level)
    {
        levelText.text = "level: " + level;
    }

    public void DisplayDamage(float damage)
    {
        damageText.text = "Damage: " + damage;
    }

    public void DisplayRange(float range)
    {
        rangeText.text = "Range: " + range;
    }

    public void DisplayUpgradeCost(float upgrade)
    {
        upgradeCostText.text = "Upgrade cost: " + upgradeCost;
    }
    public void Start()
    {
        instance = this;
    }
}
