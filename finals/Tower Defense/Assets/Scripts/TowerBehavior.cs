using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehavior : MonoBehaviour
{
    public static TowerBehavior instance;
    public int level = 1;
    public bool arrowType;
    public bool cannonType;
    public bool fireType;
    public bool iceType;
    private bool built = false;
    private string name = "Null";

    public Transform targetPosition;
    public GameObject targetObject;

    [Header("Stats")]
    public float damage = 0f;
    public float cost = 0f;
    public float upgradeCost = 0f;

    public float range = 15f;
    public float fireRate = 0f;
    private float fireCountdown = 0f;
    [Header("Others")]
    public string enemyTag = "Enemy";
    //public Transform target;

    //arrow stats
    public static int arrowDamage = 10;
    //boulder stats
    public static int boulderDamage = 7;
    //ice stats
    public static int iceDamage = 3;
    public static int iceDuration = 3;
    public static int iceSlow = 1;
    //fire stats
    public static int fireDamage = 3;
    public static int fireDuration = 2;

    public GameObject bulletPrefab;
    public Transform nozzle;




    public void TowerStats()
    {
        UIManager.instance.DisplayLevel(level);
        UIManager.instance.DisplayDamage(damage);
        UIManager.instance.DisplayRange(range);
        UIManager.instance.DisplayUpgradeCost(upgradeCost);
    }

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);

        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        //GameObject target;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            targetPosition = nearestEnemy.transform;
            targetObject = nearestEnemy;
            Shoot();
        }
        else
        {
            targetObject = null;
            targetPosition = null;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        InvokeRepeating("UpdateTarget", 0f, 0.5f);
    }
    
    // Update is called once per frame
    void Update()
    {
        if (built)
        {
            if (targetPosition == null)
            {
                return;
            }

            if (fireCountdown <= 0f)
            {
                Debug.Log("firing"); 
                Shoot();
                fireCountdown = 1f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
        }
    }

    public void BuildTower()
    {
            if (arrowType)//If tower is crossbow
            {
                Debug.Log("Crossbow built");
                StartCoroutine(BuildTower(7));
            }
            else if (cannonType)//If tower is cannon
            {
                Debug.Log("Cannon built");
                StartCoroutine(BuildTower(5));
            }
            else if (iceType)//If tower is ice
            {
                Debug.Log("Ice built");
                StartCoroutine(BuildTower(15));

            }
            else if (fireType)//If tower is fire
            {
                Debug.Log("Fire built");
                StartCoroutine(BuildTower(15));
            }
            GameManager.instance.gold -= cost;
            UIManager.instance.DisplayGold(GameManager.instance.gold);
    }

    void Shoot()
    {
        if (arrowType == true)
        {
            GameObject arrowObject = (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
            NewArrow arrow = arrowObject.GetComponent<NewArrow>();
            if (arrow != null)
            {
                arrow.Seek(targetPosition);
            }
        }
        else if (cannonType == true)
        {
            GameObject arrowObject = (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
            ArrowProjectile arrow = arrowObject.GetComponent<ArrowProjectile>();
            
            if (arrow != null)
            {
                arrow.Seek(targetPosition);
            }
        }
        else if (iceType == true)
        {
            GameObject arrowObject = (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
            //ArrowProjectile arrow = arrowObject.GetComponent<ArrowProjectile>();
            IceProjectile arrow = arrowObject.GetComponent<IceProjectile>();
                if (arrow != null)
                {
                    arrow.Seek(targetPosition);
                }
        }
        else if (fireType == true)
        {
            GameObject arrowObject = (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
            //ArrowProjectile arrow = arrowObject.GetComponent<ArrowProjectile>();
            FireProjectile arrow = arrowObject.GetComponent<FireProjectile>();
            if (arrow != null)
            {
                arrow.Seek(targetPosition);
            }
        }
    }

    public void Upgrade()
    {
        if(level < 5 && GameManager.instance.gold >= upgradeCost)
        {
            if (cannonType)
            {
                damage += damage;
                upgradeCost += 100;
            }
            if (arrowType)
            {
                damage += damage;
                upgradeCost += 100;
            }
            if (iceType)
            {
                damage += damage;
                upgradeCost += 100;
            }
            if (fireType)
            {
                damage += damage;
                upgradeCost += 100;
            }
            level++;
            TowerStats();
        }
        else
        {
            return;
        }

    }


    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    IEnumerator BuildTower(float timer)
    {
        float time = 0;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            yield return new WaitForSeconds(1);
        }
        Debug.Log("Tower is ready");
        built = true;
        this.GetComponent<TowerBehavior>().enabled = true;
    }
}
