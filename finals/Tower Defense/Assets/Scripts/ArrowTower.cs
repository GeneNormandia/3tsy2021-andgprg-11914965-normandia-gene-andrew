using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTower : MonoBehaviour
{
    [Header("Stats")]
    public float range = 15f;
    public float fireRate = 1f;
    private float fireCountdown = 0f;
    [Header("Others")]
    public string enemyTag = "Enemy";
    public Transform target;

    public static int damage = 20;




    
    public GameObject bulletPrefab;
    public Transform nozzle;

    void UpdateTarget()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag(enemyTag);
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemy in enemies)
        {
            float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemy;
            }
        }
        if (nearestEnemy != null && shortestDistance <= range)
        {
            target = nearestEnemy.transform;
           // nearestEnemy = enemy;
        }
        else
        {
            target = null;
        }
    }

    void Start()
    {
        InvokeRepeating("UpdateTarget", 0f, 0.5f);

    }

    void Update()
    {
        if (target == null)
        {
            return;
        }

        if (fireCountdown <= 0f)
        {
            Shoot();
            fireCountdown = 1f / fireRate;
        }
        fireCountdown -= Time.deltaTime;
    }

    void Shoot()
    {
        GameObject arrowObject = (GameObject)Instantiate(bulletPrefab, nozzle.position, nozzle.rotation);
        ArrowProjectile arrow = arrowObject.GetComponent<ArrowProjectile>();
        //target.GetComponent(target.Hp -= damage);
        if (arrow != null)
        {
            arrow.Seek(target);
        }
    }

   /* IEnumerator ArrowDamage(float timer)
    {
        float time = 0;
        int damage = ArrowTower.arrowDamage;
        while (timer > time)
        {
            time += 1;
            Debug.Log("time: " + time);
            yield return new WaitForSeconds(1);
        }
        EnemyMovement.Hp -= 50;
        Debug.Log("Taking Damage!");
    }*/
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
