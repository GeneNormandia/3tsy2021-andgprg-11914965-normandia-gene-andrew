using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowProjectile : MonoBehaviour
{
   
     private Transform target;
    public float explosionRadius = 0f;
    public float speed = 10f;

    public void Seek(Transform _target)
    {
            target = _target;

        
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
            return;
        }
        if (target.gameObject.GetComponent<EnemyMovement>().flying == true)
        {
            Destroy(gameObject);
            return;
        }


        Vector3 dir = target.position - transform.position;
        float distanceThisFrame = speed * Time.deltaTime;

        if (dir.magnitude <= distanceThisFrame)
        {
            HitTarget();
            return;
        }
        transform.Translate(dir.normalized * distanceThisFrame, Space.World);
        //transform.LookAt(target);
    }

    void HitTarget()
    {
        Debug.Log("HIT");
        //Destroy(gameObject);


        if (explosionRadius > 0f)
        {
            Explode();
        }
        else
        {
            target.gameObject.GetComponent<EnemyMovement>().hitBoulder();
            //target2.gameObject.GetComponent<LandBoss>().hitBoulder();
        }

        Destroy(gameObject);
    }

    void Explode()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);
        foreach (Collider collider in colliders)
        {
            if (collider.tag == "Enemy")
            {
                Damage(collider.transform);
            }
        }

    }
     
    void Damage(Transform enemy)
    {
        enemy.gameObject.GetComponent<EnemyMovement>().hitBoulder();
        Destroy(gameObject);
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
